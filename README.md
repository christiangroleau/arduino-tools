# arduino tools

![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)

A growing collection of utilities and sketches for the Arduino (mostly Uno)
