#include <SPI.h>
#include <Ethernet.h>

// MAC address of ethernet shield
byte mac[] = { 0x00, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE };

EthernetClient client;

void setup() {
  Serial.begin(9600);

  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP");

    // in error state, do nothing
    for(;;)
      ;
  }

  // print local ip address
  Serial.println(Ethernet.localIP());
}

void loop() {

}
